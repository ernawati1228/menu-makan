import { StyleSheet, Text, View, FlatList, TouchableOpacity, StatusBar} from 'react-native'
import React, {useState, useEffect} from 'react'

const App = () => {

  const[kategori, setKategori] = useState(
    [

      {
        nama: 'Ayam',
      },
      {
        nama: 'Sayur',
      },
      {
        nama: 'Minuman',
      },
      {
        nama: 'Salad',
      },
      {
        nama: 'Goringan',
      },
      {
        nama: 'Rebus',
      },
      {
        nama: 'Telur',
      },
    ]);
  
  const [kategoriSeleksi, setKategoriSeleksi] = useState({
    nama: 'Ayam',
  });

  const [dataTrending, seDataTrending] = useState([
    {namaResep: 'Ayam  Goreng Mantap', autor: 'nana'},
    {namaResep:'Ayam Goreng Rica Rica', autor: 'iki'},

  ])

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#f5f5f5" barStyle="dark-content"/>
      <View style={{marginHorizontal: 20, marginBottom: 20, marginTop: 20,}}>
        <Text style={{fontSize: 28, fontWeight: 'bold', color:'#000'}}>resepku</Text>
      </View>
      <FlatList
        data={kategori}
        horizontal
        style={{marginLeft: 10,}}
        renderItem={({item})=> (
          <TouchableOpacity
            style={{
              marginRight: 5, 
              backgroundColor: 
                kategoriSeleksi.nama == item.nama ? '#4169e1' : '#fff',
              elevation: 3,
              paddingHorizontal: 15,
              paddingVertical: 8,
              marginBottom: 10,
              borderRadius: 15,
              marginleft: 5,
            }}
            onPress={()=>setKategoriSeleksi(item)}
            >
            <Text
              style={{
                color: kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
              }}>{item.nama}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default App

const styles = StyleSheet.create({
  container: {
    justifyContent:'center',
    alignItems:'center'
  },
  nama:{
    fontSize:20,
    color:'black'
  }
})